# Računalniški Praktikum I - 2022/23

To je glavni repoyitori za predmet Racunalniski Praktikum za leto 2022/23

[E-učilnica] (https://e.famnit.upr.si/course/view.php?id=4933)

 ## Git ukazi

 | Ukaz | kratek opis |
 | - | - |
 |git add | izberemo dadtoteke ki jih želimo sahraniti (commitati). |
 | git commit -m "message" | Lokalno sahrani izbrane datoteke. |
 | git push | Lokalno shranjene datoteke pošljimo na oddaljen repozitorij. |