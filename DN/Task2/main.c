#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(){
    srand(time(NULL));

    char guessWords [][16] = {
        "gitlab",
        "computer",
        "banana",
        "bioinformatics",
        "linux",
        "sudoku"
};
    int randomIndex = rand() % 6;

    printf("\n\t - All alphabet are in lower case.");
    printf("\n\t - If you enjoy continue, otherwise quit it.");

    int numLives = 10;
    int numCorrect = 0;
    int oldCorrect = 0;

    int lengthOfWord = strlen(guessWords[randomIndex]);
    int letterGuessed[8] = {0,0,0,0,0,0,0,0,};

    int quit = 0;

    int i = 0;

    char guess[16];
    char letterEntered;

    /*printf("guessWords: %s randomIndex:%d lengthOfWord:%d\n",
            guessWords[randomIndex],
            randomIndex,
            lengthOfWord);*/

    while (numCorrect < lengthOfWord)
    {
        
        printf("New Turn...\nHangman Word:");
        for ( i = 0; i < lengthOfWord; i++)
        {
            if (letterGuessed[i] == 1)
            {
                printf("%c", guessWords[randomIndex][i]);
            }else{
                printf("_");
            }
            
            
        }
        printf("\n");


        printf("Number Correct so far:%d\n", numCorrect);
        printf("Enter a guess letter:");
        fgets(guess, 16, stdin);

        if ( strncmp(guess, "quit", 4) == 0)
        {
            quit = 1;
            break;
        }

        letterEntered = guess[0];

        printf("letterEntered: %c\n", letterEntered);

        oldCorrect = numCorrect;

        for ( i = 0; i < lengthOfWord; i++)
        {
            if (letterGuessed[i] == 1)
            {
                continue;
            }
            
            if (letterEntered == guessWords[randomIndex][i])
            {
                letterGuessed[i] = 1;
                numCorrect++;
            }
            
        }
        if (oldCorrect == numCorrect)
        {
            numLives--;
            printf("Sorry wrong guess\n");
            if (numLives == 0)
            {
                break;
            }
            
        }else {
            printf("Correct guess\n");
        }
        
        
    }
    if (quit == 1)
    {
        printf("The user quit early\n");
    }else if (numLives == 0)
    {
        printf("\nSorry you lose, the word was: %s\n",
        guessWords[randomIndex]);
    }else {
        printf("\nYou win!\n");
    }
    
    

    
    return 0;
}